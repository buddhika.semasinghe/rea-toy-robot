package com.buddhika.assignments;

public enum Direction {
    NORTH, EAST, SOUTH, WEST
}
