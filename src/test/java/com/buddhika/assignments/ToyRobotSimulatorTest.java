package com.buddhika.assignments;

import org.junit.Assert;
import org.junit.Test;

public class ToyRobotSimulatorTest {

    @Test
    public void testPlaceInitialPositionShouldReportSamePosition(){
        RobotPosition initialPosition = new RobotPosition();

        ToyRobotSimulator toyRobotSimulator = new ToyRobotSimulator();
        toyRobotSimulator.place(initialPosition);

        RobotPosition robotPosition = toyRobotSimulator.report();
        Assert.assertEquals(initialPosition.getxPosition(), robotPosition.getxPosition());
        Assert.assertEquals(initialPosition.getyPosition(), robotPosition.getyPosition());
        Assert.assertEquals(initialPosition.getFacingOn(), robotPosition.getFacingOn());
    }
}
