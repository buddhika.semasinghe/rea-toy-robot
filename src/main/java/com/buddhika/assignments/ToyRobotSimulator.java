package com.buddhika.assignments;

public class ToyRobotSimulator {
    private RobotPosition currentPosition;

    public void place(RobotPosition initialPosition) {
        this.currentPosition = initialPosition;
    }

    public RobotPosition report() {
        return currentPosition;
    }
}
