package com.buddhika.assignments;

public class RobotPosition {
    private int xPosition;
    private int yPosition;
    private Direction facingOn;

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public Direction getFacingOn() {
        return facingOn;
    }

    public void setFacingOn(Direction facingOn) {
        this.facingOn = facingOn;
    }
}
